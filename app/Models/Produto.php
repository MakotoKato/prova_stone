<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Categoria;
use App\Models\Pedido;

class Produto extends Model
{
	protected $table = "produtos";

	protected $fillable = [
        'categoria_id',
        'titulo',
        'descricao',
        'preco',
        'imagem',
	]; 

	public function categoria(){
		return $this->belongsTo(Categoria::class);
	}

	public function pedidos(){
		return $this->belongsToMany(Pedido::class,'produtos_pedidos','produto_id','pedido_id');
	}
}
