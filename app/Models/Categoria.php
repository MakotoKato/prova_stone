<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produto;

class Categoria extends Model
{
	protected $table = "categorias";

	protected $fillable = [
		"nome",
	];

	public function produtos(){
		return $this->hasMany(Produto::class);
	}
}
