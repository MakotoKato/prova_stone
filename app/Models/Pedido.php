<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produto;

class Pedido extends Model
{
	protected $table = "pedidos";

	protected $fillable = [
		"aberto",
		"nome",
		"email",
		"prazo",
		"frete",
	];

	public function produtos(){
		return $this->belongsToMany(Produto::class,'produtos_pedidos','pedido_id','produto_id');
	}
}
