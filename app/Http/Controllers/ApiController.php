<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
	public function prazo(Request $request){
		return response()->json(rand(1,20));
	}

	public function frete(Request $request){
		return response()->json(rand(0,10000));
	}
}
