<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
	public function index(Request $request){
		return view('admin.index');
	}

	public function categoria(Request $request){
		return view('admin.categoria.index');
	}

	public function produto(Request $request){
		return view('admin.produto.index');
	}

	public function venda(Request $request){
		return view('admin.vendas.index');
	}
}
