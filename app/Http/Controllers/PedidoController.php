<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pedido;

class PedidoController extends Controller
{
	public function create(Request $request){
	
		$pedido = Pedido::create();
		$pedido->produtos()->attach($request->only('id'));
		$pedido->produtos;
		return response()->json($pedido);
	}

	public function last(){
		$pedido = Pedido::where('aberto',true)->with('produtos')->latest()->first();
		if(empty($pedido)){
			$pedido = false;
		}
		return response()->json($pedido);		
	}

	public function close(Request $request){
		$closed = false;
		$pedido = Pedido::find($request->only('id'))->first();
		$pedidoFill = $request->only('nome','email','prazo','frete');

		if($pedido and $pedidoFill['nome'] and $pedidoFill['email']){

			$pedido->nome = $pedidoFill['nome'];
			$pedido->email = $pedidoFill['email'];
			$pedido->prazo = $pedidoFill['prazo'];
			$pedido->frete = $pedidoFill['frete'];
			$pedido->aberto = false;
			$closed = $pedido->save();
		}

		return response()->json($closed);
	}

	public function read(Request $request){
		$pedidos = Pedido::with('produtos')->where('aberto',false)->get();
		return response()->json($pedidos);
	}

	public function update(Request $request){
		$pedido = Pedido::with('produtos')->find($request->only('id'))->first();

		$adds = $request->only('add');
		$subs = $request->only('sub');
		if($adds){
			$pedido->produtos()->attach($adds['add']['id']);
		}
		if($subs){


			$duplicates = ($pedido->produtos->where('id',$subs['sub']['id'])->count() - 1);
			$pedido->produtos()->detach($subs['sub']['id']);

			if($duplicates > 0){
				for($i=0;$i<$duplicates;$i++){
					$pedido->produtos()->attach($subs['sub']['id']);
				}
			}

		}

		$pedido = Pedido::with('produtos')->latest()->first();

		return response()->json($pedido);
	}

	public function delete(Request $request){
		return response()->json(true);
	}
}
