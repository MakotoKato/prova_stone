<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;

class CategoriaController extends Controller
{
	public function create(Request $request){
		
		try{
			Categoria::create($request->only('nome'));
			$return = true;
		}
		catch (\Illuminate\Database\QueryException $e) {
    		//dd($e->errorInfo);
    		$return = false;
		}

		return response()->json($return);
	}

	public function read(Request $request){

		return response()->json(Categoria::all());
	}

	public function update(Request $request){
		$updated = Categoria::where('id', $request->only('id'))->update($request->only('nome'));
		return response()->json($updated);
	}

	public function delete(Request $request, Categoria $categoria){
		try{
			$deleted = $categoria->delete();
		}
		catch (\Illuminate\Database\QueryException $e) {
    		//dd($e->errorInfo);
    		$deleted = false;
		}
		
		return response()->json($deleted);
	}
}
