<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Produto;

class ProdutoController extends Controller
{
	public function create(Request $request){
		$path = Storage::put('/public', $request->file('imagem'), 'public');

		$produtoFillable = $request->only('titulo','descricao','preco','categoria_id');
		$produtoFillable['imagem'] = str_replace('public', 'storage', $path);

		$produto = Produto::create($produtoFillable);

		return response()->json(true);
	}

	public function read(Request $request){
		return response()->json(Produto::with('categoria')->get());
	}

	public function update(Request $request){
		$updated = false;
		$produto = Produto::find($request->only('id'))->first();
		if($produto){
			$produtoFillable = $request->only('titulo','descricao','preco','categoria_id');
			try{
				Storage::delete(str_replace('storage','public',$produto->imagem));
			}
			catch(exception $e){

			}
			$path = Storage::put('/public', $request->file('imagem'), 'public');
			$produtoFillable['imagem'] = str_replace('public', 'storage', $path);
			$updated = $produto->update($produtoFillable);
		}
		return response()->json($updated);
	}



	public function delete(Request $request, Produto $produto){
		try{
			$deleted = Storage::delete(str_replace('storage','public',$produto->imagem));
			$deleted = $deleted and $produto->delete();
		}
		catch (\Illuminate\Database\QueryException $e) {
    		//dd($e->errorInfo);
    		$deleted = false;
		}
		
		return response()->json($deleted);
	}
}
