@extends('partials.base')
    <link href="{{ asset('node_modules/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
@section('head')
@endsection

@section('body')

	@include('partials.topbar')

    <div class="container-fluid">
    	<div class="row">

    		@include('admin.partials.sidebar')

    	</div>
    </div>

@endsection

@section('foot')
    <script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
@endsection