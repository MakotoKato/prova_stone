@extends('partials.base')
    <link href="{{ asset('node_modules/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
@section('head')
@endsection

@section('body')

	@include('partials.topbar')

    <div class="container-fluid">
    	<div class="row">

    		@include('admin.partials.sidebar')

			<main ng-app="categoria" role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
				<div ui-view name="content"></div>
			</main>
    	</div>
    </div>

@endsection

@section('foot')
    <script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('node_modules/angular/angular.js') }}"></script>
    <script src="{{ asset('node_modules/@uirouter/angularjs/release/angular-ui-router.min.js') }}"></script>
    <script src="{{ asset('js/angular/admin/categoria/categoria.js') }}"></script>
    <script src="{{ asset('js/angular/admin/categoria/controllers/read.js') }}"></script>
    <script src="{{ asset('js/angular/admin/categoria/controllers/create.js') }}"></script>
    <script src="{{ asset('js/angular/admin/categoria/controllers/update.js') }}"></script>
    <script src="{{ asset('js/angular/admin/categoria/controllers/delete.js') }}"></script>
@endsection