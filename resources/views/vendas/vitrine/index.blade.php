@extends('partials.base')
    <link href="{{ asset('node_modules/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
@section('head')
@endsection

@section('body')
	@include('partials.topbar')
    



	<main ng-app="vitrine" role="main">
		<div ui-view name="content"></div>
	</main>




@endsection

@section('foot')
    <script src="{{ asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('node_modules/angular/angular.js') }}"></script>
    <script src="{{ asset('node_modules/@uirouter/angularjs/release/angular-ui-router.min.js') }}"></script>
    <script src="{{ asset('js/angular/vendas/vitrine/vitrine.js') }}"></script>
    <script src="{{ asset('js/angular/vendas/vitrine/controllers/index.js') }}"></script>
    <script src="{{ asset('js/angular/vendas/vitrine/controllers/carrinho.js') }}"></script>
@endsection