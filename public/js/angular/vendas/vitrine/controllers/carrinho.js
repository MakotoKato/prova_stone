angular
.module('vitrine')
.controller('carrinho', carrinho);

carrinho.$inject = ['routes','$state', '$rootScope', '$stateParams', '$scope'];
function carrinho(routes,$state, $rootScope, $stateParams, $scope)
{
	$scope.pedido = false;

	routes.lastPedido().then(function(data){
		$scope.pedido = data.data;
		if(!$scope.pedido){
			$state.go('index', {message:{
				text:"carrinho vazio",
				class:"alert alert-warning",
			}});
		}
	});

	$scope.fecharPedido = function(){
		routes.getFrete().then(function(data){
			$scope.pedido.frete = data.data;
		});
		routes.getPrazo().then(function(data){
			$scope.pedido.prazo = data.data;
		});
	};

	$scope.confirmarFechamento = function(){
		routes.closePedido($scope.pedido).then(function(data){
			if(data.data==true){
				$state.go('index', {message:{
					text:"Pedido Confirmado, Agradecemos sua preferência!",
					class:"alert alert-success",
				}});
			}
			else{
				$scope.message = {
					text:"Pedido não confirmado...",
					class:"alert alert-danger",
				};
			}		
		});

	};


	$scope.remover = function(produto){
		$scope.pedido.sub = produto;
		routes.updatePedidos($scope.pedido).then(function(data){
			$scope.pedido = data.data;
		});
	};

	$scope.valorPedido = function(){

		let valor = 0;
		angular.forEach($scope.pedido.produtos, function(produto, key) {
		  valor = valor + produto.preco;
		});
		return valor;
	}

	$scope.valorTotal = function(){
		return $scope.pedido.frete + $scope.valorPedido();
	}

	console.log($scope.pedido);
}