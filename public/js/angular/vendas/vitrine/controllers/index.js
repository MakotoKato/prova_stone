angular
.module('vitrine')
.controller('index', index);

index.$inject = ['routes','$state', '$rootScope', '$stateParams', '$scope'];
function index(routes,$state, $rootScope, $stateParams, $scope)
{
	$scope.categorias = [];
	$scope.produtos = [];
	$scope.pedido = false;
	$scope.produtosEmVitrine = [];
	$scope.message = $stateParams.message;
	$scope.pagina = 0;
	$scope.maxProdutos = 4;


	routes.readCategorias().then(function(data){
		$scope.categorias = data.data;
	});

	routes.readProdutos().then(function(data){
		$scope.produtos = data.data;
		$scope.paginar();
	});

	routes.lastPedido().then(function(data){
		$scope.pedido = data.data;
	});

	$scope.paginar = function(){
		$scope.produtosEmVitrine = $scope.produtos.slice(
			($scope.pagina)*($scope.maxProdutos),
			($scope.pagina+1)*($scope.maxProdutos)
		);
	};

	$scope.avancar = function(){
		if($scope.pagina < (($scope.produtos.length/$scope.maxProdutos)-1)) {
			$scope.pagina = $scope.pagina+1;
			$scope.paginar();
		}
	};

	$scope.retornar = function(){
		if($scope.pagina > 0){
			$scope.pagina = $scope.pagina-1;
			$scope.paginar();
		}
	};

	$scope.comprar = function(produto){
		if($scope.pedido){
			$scope.pedido.add = produto;
			routes.updatePedidos($scope.pedido).then(function(data){
				$scope.pedido = data.data;
			});
		}
		else{
			routes.createPedidos(produto).then(function(data){
				$scope.pedido = data.data;
			});
		}
	};
}