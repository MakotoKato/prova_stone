angular
.module('vitrine',['ui.router'])
.service('routes',['$http',function(http){
	return ({
		readCategorias : function(){
			return http.get('/api/categoria/read');
		},
		readProdutos : function(){
			return http.get('/api/produto/read');
		},
		closePedido : function($formData){
			return http.post('/api/pedido/close',$formData);
		},		
		lastPedido : function(){
			return http.get('/api/pedido/last');
		},
		createPedidos : function($formData){
			return http.post('/api/pedido/create',$formData);
		},
		updatePedidos : function($formData){
			return http.put('/api/pedido/update',$formData);
		},
		getPrazo : function(){
			return http.get('/api/prazo');
		},
		getFrete : function(){
			return http.get('/api/frete');
		},
	});
}])
.config(config);

config.$inject = ['$stateProvider','$urlRouterProvider'];

function config($stateProvider,$urlRouterProvider){
	$root = '/js/angular/vendas/vitrine/views/';
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('index', {
				url: '/',
				params:{message:null},
				views: {
					content: {
						templateUrl: $root + 'index.html',
						controller: 'index'
					}
				}
		})
		.state('carrinho', {
				url: '/carrinho',
				views: {
					content: {
						templateUrl: $root + 'carrinho.html',
						controller: 'carrinho'
					}
				}
		})
}