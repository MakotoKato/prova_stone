angular
.module('categoria')
.controller('read_categoria', read_categoria);

read_categoria.$inject = ['routes','$state', '$rootScope', '$stateParams', '$scope'];
function read_categoria(routes,$state, $rootScope, $stateParams, $scope)
{
	$scope.categorias = [];
	$scope.message = $stateParams.message;

	routes.readCategorias().then(function(data){
		$scope.categorias = data.data;
	});

	$scope.create = function(){
		$state.go('create');
	};	

	$scope.update = function(categoria){
		$state.go('update', {categoria:categoria});
	};

	$scope.delete = function(categoria){
		$state.go('delete', {categoria:categoria});
	};
}