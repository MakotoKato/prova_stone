angular
.module('categoria')
.controller('delete_categoria', delete_categoria);

delete_categoria.$inject = ['$state', '$rootScope', '$scope','$stateParams','routes'];
function delete_categoria($state, $rootScope, $scope, $stateParams, routes)
{
	if(!$stateParams.categoria){
		$state.go('read');
	}
	
	$scope.categoria = $stateParams.categoria;


	$scope.send = function(){
		routes.deleteCategorias($scope.categoria).then(function(data){
			if(data.data == true){
				$scope.success();
			}
			else{
				$scope.fail();
			}
		});
	};

	$scope.success = function(){
		$state.go('read', {message:{
			text:"categoria deletada com sucesso!",
			class:"alert alert-success",
		}});
	};

	$scope.fail = function(){
		$state.go('read', {message:{
			text:"deleção de categoria sem sucesso...",
			class:"alert alert-danger",
		}});
	};
}