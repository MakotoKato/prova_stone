angular
.module('categoria')
.controller('update_categoria', update_categoria);

update_categoria.$inject = ['$state', '$rootScope', '$scope','$stateParams','routes'];
function update_categoria($state, $rootScope, $scope, $stateParams, routes)
{
	if(!$stateParams.categoria){
		$state.go('read');
	}

	$scope.categoria = $stateParams.categoria;

	$scope.send = function(){
		routes.updateCategorias($scope.categoria).then(function(data){
			if(data.data == true){
				$scope.success();
			}
			else{
				$scope.fail();
			}
		});
	};

	$scope.check = function(){

		if($scope.categoria || $scope.categoria.nome){
			return false;
		}
		else{
			return true;
		}
	};

	$scope.success = function(){
		$state.go('read', {message:{
			text:"categoria alterada com sucesso!",
			class:"alert alert-success",
		}});
	};

	$scope.fail = function(){
		$state.go('read', {message:{
			text:"alteração de categoria sem sucesso...",
			class:"alert alert-danger",
		}});
	};
}