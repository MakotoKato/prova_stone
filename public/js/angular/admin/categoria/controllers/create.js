angular
.module('categoria')
.controller('create_categoria', create_categoria);

create_categoria.$inject = ['routes','$state', '$rootScope', '$scope'];
function create_categoria(routes,$state, $rootScope, $scope)
{
	$scope.categoria = {
		"nome" : "",
	};

	$scope.send = function(){
		routes.createCategorias($scope.categoria).then(function(data){
			if(data.data == true){
				$scope.success();
			}
			else{
				$scope.fail();
			}
		});
	};

	$scope.check = function(){
		if($scope.categoria.nome){
			return false;
		}
		else{
			return true;
		}
	};

	$scope.success = function(){
		$state.go('read', {message:{
			text:"categoria inclusa com sucesso!",
			class:"alert alert-success",
		}});
	};

	$scope.fail = function(){
		$state.go('read', {message:{
			text:"inclusão de categoria sem sucesso...",
			class:"alert alert-danger",
		}});
	};
}