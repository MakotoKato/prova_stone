angular
.module('categoria',['ui.router'])
.service('routes',['$http',function(http){
	return ({
		readCategorias : function(){
			return http.get('/api/categoria/read');
		},
		createCategorias : function($formData){
			return http.post('/api/categoria/create',$formData);
		},
		updateCategorias : function($formData){
			return http.put('/api/categoria/update',$formData);
		},
		deleteCategorias : function($formData){
			return http.delete('/api/categoria/delete/'+$formData.id);
		}
	});
}])
.config(config);

config.$inject = ['$stateProvider','$urlRouterProvider'];

function config($stateProvider,$urlRouterProvider){
	$root = '/js/angular/admin/categoria/views/';
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('read', {
				url: '/',
				params:{message:null},
				views: {
					content: {
						templateUrl: $root + 'read.html',
						controller: 'read_categoria'
					}
				}
		})
		.state('create', {
				url: '/create',
				views: {
					content: {
						templateUrl: $root + 'create.html',
						controller: 'create_categoria'
					}
				}
		})
		.state('update', {
				url: '/update',
				params:{categoria:null},
				views: {
					content: {
						templateUrl: $root + 'update.html',
						controller: 'update_categoria'
					}
				}
		})
		.state('delete', {
				url: '/delete',
				params:{categoria:null},
				views: {
					content: {
						templateUrl: $root + 'delete.html',
						controller: 'delete_categoria'
					}
				}
		})
}