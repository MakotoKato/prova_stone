angular
.module('produto')
.controller('update_produto', update_produto);

update_produto.$inject = ['$state', '$rootScope', '$scope','$stateParams','routes'];
function update_produto($state, $rootScope, $scope, $stateParams, routes)
{
	if(!$stateParams.produto){
		$state.go('read');
	}
	$scope.imagem = null;

	$scope.produto = $stateParams.produto;
	$scope.categorias = [];
	routes.readCategorias().then(function(data){
		$scope.categorias = data.data;
	});

	$scope.send = function(){
		routes.updateProdutos($scope.produto,$scope.imagem).then(function(data){
			if(data.data == true){
				$scope.success();
			}
			else{
				$scope.fail();
			}
		});
	};

	$scope.check = function(){
		if( $scope.produto.titulo &&
			$scope.produto.descricao &&
			$scope.produto.preco &&
			$scope.produto.categoria_id &&
			$scope.imagem)
		{
			return false;
		}
		else{
			return true;
		}
	};

	$scope.success = function(){
		$state.go('read', {message:{
			text:"produto alterado com sucesso!",
			class:"alert alert-success",
		}});
	};

	$scope.fail = function(){
		$state.go('read', {message:{
			text:"alteração de produto sem sucesso...",
			class:"alert alert-danger",
		}});
	};
}