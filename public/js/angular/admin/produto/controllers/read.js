angular
.module('produto')
.controller('read_produto', read_produto);

read_produto.$inject = ['routes','$state', '$rootScope', '$stateParams', '$scope'];
function read_produto(routes,$state, $rootScope, $stateParams, $scope)
{
	$scope.produtos = [];
	$scope.message = $stateParams.message;

	routes.readProdutos().then(function(data){
		$scope.produtos = data.data;
	});

	$scope.create = function(){
		$state.go('create');
	};	

	$scope.update = function(produto){
		$state.go('update', {produto:produto});
	};

	$scope.delete = function(produto){
		$state.go('delete', {produto:produto});
	};
}