angular
.module('produto')
.controller('create_produto', create_produto);

create_produto.$inject = ['routes','$state', '$rootScope', '$scope'];
function create_produto(routes,$state, $rootScope, $scope)
{
	$scope.imagem = null;
	$scope.produto = {
		"titulo" : "",
		"descricao" : "",
		"preco" : "",
		"categoria_id" : ""
	};

	$scope.categorias = [];
	routes.readCategorias().then(function(data){
		$scope.categorias = data.data;
	});

	$scope.send = function(){
		routes.createProdutos($scope.produto,$scope.imagem).then(function(data){
			if(data.data == true){
				$scope.success();
			}
			else{
				$scope.fail();
			}
		});
	};

	$scope.check = function(){
		if( $scope.produto.titulo &&
			$scope.produto.descricao &&
			$scope.produto.preco &&
			$scope.produto.categoria_id &&
			$scope.imagem)
		{
			return false;
		}
		else{
			return true;
		}
	};

	$scope.success = function(){
		$state.go('read', {message:{
			text:"produto incluso com sucesso!",
			class:"alert alert-success",
		}});
	};

	$scope.fail = function(){
		$state.go('read', {message:{
			text:"inclusão de produto sem sucesso...",
			class:"alert alert-danger",
		}});
	};
}