angular
.module('produto')
.controller('delete_produto', delete_produto);

delete_produto.$inject = ['$state', '$rootScope', '$scope','$stateParams','routes'];
function delete_produto($state, $rootScope, $scope, $stateParams, routes)
{
	if(!$stateParams.produto){
		$state.go('read');
	}
	
	$scope.produto = $stateParams.produto;


	$scope.send = function(){
		routes.deleteProdutos($scope.produto).then(function(data){
			if(data.data == true){
				$scope.success();
			}
			else{
				$scope.fail();
			}
		});
	};

	$scope.success = function(){
		$state.go('read', {message:{
			text:"produto deletado com sucesso!",
			class:"alert alert-success",
		}});
	};

	$scope.fail = function(){
		$state.go('read', {message:{
			text:"deleção de produto sem sucesso...",
			class:"alert alert-danger",
		}});
	};
}