angular
.module('produto',['ui.router'])
.directive('fileModel',fileModel)
.service('routes',['$http',function(http){
	return ({
		readCategorias : function(){
			return http.get('/api/categoria/read');
		},
		readProdutos : function(){
			return http.get('/api/produto/read');
		},
		createProdutos : function($formData,file){
			
			var fd = new FormData();
			fd.append('imagem', file);

			for ( var key in $formData) {
	    		fd.append(key, $formData[key]);
			}

	        return http.post('/api/produto/create', fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined,'Process-Data': false}
	        });
		},
		updateProdutos : function($formData,file){

			var fd = new FormData();
			fd.append('imagem', file);

			for ( var key in $formData) {
	    		fd.append(key, $formData[key]);
			}

			return http.post('/api/produto/update', fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined,'Process-Data': false}
	        });
		},
		deleteProdutos : function($formData){
			return http.delete('/api/produto/delete/'+$formData.id);
		}
	});
}])
.config(config);

config.$inject = ['$stateProvider','$urlRouterProvider'];
function config($stateProvider,$urlRouterProvider){
	$root = '/js/angular/admin/produto/views/';
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('read', {
				url: '/',
				params:{message:null},
				views: {
					content: {
						templateUrl: $root + 'read.html',
						controller: 'read_produto'
					}
				}
		})
		.state('create', {
				url: '/create',
				views: {
					content: {
						templateUrl: $root + 'create.html',
						controller: 'create_produto'
					}
				}
		})
		.state('update', {
				url: '/update',
				params:{produto:null},
				views: {
					content: {
						templateUrl: $root + 'update.html',
						controller: 'update_produto'
					}
				}
		})
		.state('delete', {
				url: '/delete',
				params:{produto:null},
				views: {
					content: {
						templateUrl: $root + 'delete.html',
						controller: 'delete_produto'
					}
				}
		})
}

fileModel.$inject = ['$parse'];
function fileModel($parse){
    return {
    	restrict: 'A',
    	link: function(scope, element, attrs) {
        	var model = $parse(attrs.fileModel);
        	var modelSetter = model.assign;

        	element.bind('change', function(){
            	scope.$apply(function(){
                	modelSetter(scope, element[0].files[0]);
            	});
        	});
    	}
	};
}