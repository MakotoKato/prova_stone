angular
.module('venda',['ui.router'])
.service('routes',['$http',function(http){
	return ({
		readPedidos : function(){
			return http.get('/api/pedido/read');
		}
	});
}])
.config(config);

config.$inject = ['$stateProvider','$urlRouterProvider'];

function config($stateProvider,$urlRouterProvider){
	$root = '/js/angular/admin/venda/views/';
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('read', {
				url: '/',
				params:{message:null},
				views: {
					content: {
						templateUrl: $root + 'read.html',
						controller: 'read_venda'
					}
				}
		})
}