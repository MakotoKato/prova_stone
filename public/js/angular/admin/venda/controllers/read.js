angular
.module('venda')
.controller('read_venda', read_venda);

read_venda.$inject = ['routes','$state', '$rootScope', '$stateParams', '$scope'];
function read_venda(routes,$state, $rootScope, $stateParams, $scope)
{
	routes.readPedidos().then(function(data){
		$scope.pedidos = data.data;
	});

	$scope.valorItens = function(pedido){
		let valor = 0;
		angular.forEach(pedido.produtos, function(produto, key) {
		  valor = valor + produto.preco;
		});
		return valor;
	};

}