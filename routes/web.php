<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','VendasController@vitrine');

Route::get('/admin','AdminController@venda');
Route::get('/admin/categoria','AdminController@categoria');
Route::get('/admin/produto','AdminController@produto');
Route::get('/admin/venda','AdminController@venda');