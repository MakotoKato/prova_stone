<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/frete','ApiController@frete');
Route::get('/prazo','ApiController@prazo');

Route::post('/categoria/create','CategoriaController@create');
Route::get('/categoria/read','CategoriaController@read');
Route::put('/categoria/update','CategoriaController@update');
Route::delete('/categoria/delete/{categoria}','CategoriaController@delete');

Route::post('/produto/create','ProdutoController@create');
Route::get('/produto/read','ProdutoController@read');
Route::post('/produto/update','ProdutoController@update');
Route::delete('/produto/delete/{produto}','ProdutoController@delete');

Route::post('/pedido/create','PedidoController@create');
Route::post('/pedido/close','PedidoController@close');
Route::get('/pedido/last','PedidoController@last');
Route::get('/pedido/read','PedidoController@read');
Route::put('/pedido/update','PedidoController@update');
Route::delete('/pedido/delete','PedidoController@delete');
